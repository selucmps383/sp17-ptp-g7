### What is an API? ###
* API stand for Application Programing Interface.   
* This means that third party can write code that interfaces with other code.   
* A Web Service is a type of API. Commonly, API communicate using HTTP protocol.   
* For example:
    1.  SOAP(*S*imple *O*bject *A*ccess *P*rotocol)
    2.  REST(*RE*presentational *S*tate *T*ransfer)
    3.  WSDL( *W*eb *S*ervices *D*escription *L*anguage)
    4.  JAX-RS(Java API for RESTful Web Services)
    5.  JAX-WS(Java API for XML Web Services) …Etc. 
> Starting development without a proper architectural design perspective, approach, or consideration could lead to unneeded redundancy, complexity, and purposeless constraints.  
[A Tale of Four API Designs: Dissecting Common API Architectures](http://nordicapis.com/a-tale-of-four-api-designs-dissecting-common-api-architectures/)  

### REST API, what is it? ###
 > Representational State Transfer (REST) architectural style for distributed hypermedia systems, describing the software engineering principles guiding REST and the interaction constraints chosen to retain those principles, while contrasting them to the constraints of other architectural styles. REST is a hybrid style derived from several of the network-based architectural styles combined with additional constraints that define a uniform connector interface. 
 [Learn more](http://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm)
 
### RESTful API Design Definitions ###
* Resource: A single instance of an object.  
* Collection: A collection of homogeneous objects.   
* HTTP: A protocol for communicating over a network.  
* Consumer: A client computer application capable of making HTTP requests.  
* Third Party Developer: A developer not a part of your project but who wishes to consume your data.  
* Server: An HTTP server/application accessible from a Consumer over a network.  
* Endpoint: An API URL on a Server which represents either a Resource or an entire Collection.  
* Idempotent: Side-effect free, can happen multiple times without penalty.  
* URL Segment: A slash-separated piece of information in the URL.   
 
### CHARACTERISTICS  
* Resource  
    * URI  
    * UNIFORM INTERFACE 
        * Methods
        * Representations
            * How resources get manipulated in a RESTful api or architecture.  
            * Representation data files between client and server (XML,JSON).
* Protocol  
    * Client-server
    * Stateless
    * Cacheable
    * Layered  
	
### Uniform Interface  
>![clientserver](/img/client-server.png)  
* Defines the interface between client and server  
* Simplifies and decoupled the architecture  
* Fundamental to RESTful design  
* HTTP verbs(GET,PUT,POST,DELETE)  
* URIs(resource name)  
* HTTP response(status,body)  
 
### HTTP Request Anatomy    
* Request Method    
* Headers  
* Request Body  

### HTTP Response Anatomy  
* Headers     
* Body  
### HTTP Response Code  
[Learn more!](http://www.restapitutorial.com/httpstatuscodes.html)  
* 1XX: Informational  
* A Status Code that starts with 1 means informational. This means that the request from the browser is still processing.  
	* Ex: 100 Continue  
* 2XX: Success  
* A Status Code that starts with 2 means success. This means that the request was successful.  
	* Most Important: 200 OK  
	* 203 Non-Authoritative Information  
	* 206 Partial Content  
* 3XX: Something has moved and tell where is the new location(relocation)  
* A Status Code that starts with 3 means redirection. This means that the request has an action that needs to be completed.  
* 4XX: Client error  
* A Status Code that starts with 4 means client error. This means that the request contains a syntax error.  
	* 404 resource was not found (the request is not formatted properly)  
	* 400 Bad Request  
	* 403 Forbidden  
* 5XX Server Error  
	* 500 Internal Server Error  
	* 503 Service Unavailable  
* Using HTTP Methods for RESTful Services  
* HTTP (Hypertext Transfer Protcol) Status Codes occur when a user submits a URL into the browser. When the User does 
this the browser then sends the information to the server. The server then sends back the HTTP Status Code.
* HTTP Status codes are only three digits long. The first digit classifies the HTTP Status Code.
* A Status Code that start with 5 means servor error. This means that the request the sever cannot complete the request. 
* Popular HTTP Status Codes consist of 200 OK, 400 Bad Request, 401 Unauthorized, and 404 Not Found.
```
GET (SELECT): Retrieve a specific Resource from the Server, or a listing of Resources.  
GET http://www.example.com/customers/12345
GET http://www.example.com/customers/12345/orders
GET http://www.example.com/buckets/sample
```
```
POST (CREATE): Create a new Resource on the Server.
POST http://www.example.com/customers
POST http://www.example.com/customers/12345/orders  
```
```
PUT (UPDATE): Update a Resource on the Server, providing the entire Resource.
PUT http://www.example.com/customers/12345
PUT http://www.example.com/customers/12345/orders/98765
PUT http://www.example.com/buckets/secret_stuff
```
```
PATCH (UPDATE): Update a Resource on the Server, providing only changed attributes.
PATCH http://www.example.com/customers/12345
PATCH http://www.example.com/customers/12345/orders/98765
PATCH http://www.example.com/buckets/secret_stuff
```
```
DELETE (DELETE): Remove a Resource from the Server.
DELETE http://www.example.com/customers/12345
DELETE http://www.example.com/customers/12345/orders
DELETE http://www.example.com/bucket/sample  
```
* Here are two lesser known HTTP verbs:
* HEAD – Retrieve meta data about a Resource, such as a hash of the data or when it was last updated.  
* OPTIONS – Retrieve information about what the Consumer is allowed to do with the Resource.

### STATELESS (COOKIES) 
* Server constrains no client state    
    * Each request contains enough context to server process the message  
        *Self-descriptive messages  
* Any session sate is held on the client    
>![LoadBalancing](/img/Load-Balancing.png)  

### CLIENT-SERVER
* Assume a disconnected system.     
* Separation of concerns.  
* Uniform inter face is the link between the 2.  
>![REST](/img/rest.png) 

### CACHEABLE
* Server response (representational) are cacheable  
    * Implicitly  
    * Explicitly  
    * Negotiated  
### LAYERED-SYSTEM
* Client can’t assume direct connection to server.  
* Software or hardware intermediaries between client and server.  
* Improve scalability.  
>![TCPIP](/img/TCP-IP-model-vs-OSI-model.png)   
### CODE ON DEMAND
* Server can temporarily extend client.  
* Transfer logic to client.  

### IDEMPOTENCE
* Request can be done multiple times  
* _GET_ is idempotent  
* Can read multiple times and return the same result.   
* An HTTP GET should never be used to change data.    
* _PUT_ is idempotent   
* Can update multiple times(with same data) and have the same end result.   
* _DELETE_ is idempotent according to the HTTP spec.  
* _POST_ is not idempotent

### OAuth 2.0  
[Learn more](https://oauth.net/2/)  
1. Better control over resources in user’s account.   
2. Revoke access to individual app.  
3. Change password without revoking access by app.  
4. Does not require username&password.  
Create a key that hand-off to a specific app to access your account.  
>![OAUTH](/img/OAUTH.png)

### JWT ###

* JWT (JSON Web Token) takes data and secures it.
* JWT's contain information that it sends to the server to be verified.
* JWT's are used when the user tries to login to their account. When the user logins in the server creates
a JWT and then returns it. The JWT is then sent to be authorized and recieves the information. After this the 
response to then sent to the user.
* JWT's consist of three parts: the header, payload, and signature. Each separated by a dot.
* header.payload.signature
* The header is made up of two parts: the type and the algorithm used.
>![URI](http://i68.tinypic.com/mah2ec.png)
* The payload contains the claims. The claims can be registered, public, or private.
* Registered claims are not mandatory and conist of the issuer, expiration time, subject, and others.
* Public claims use information submitted by the user, such as the username.
* Private claims are claims created between parties that have agreed to use them.
>![URI](http://i63.tinypic.com/2f081md.png)
* The signature consists of the algorithm used in the header, the encoded header, the encoded payload, signed, and a secret.
* The secret is a key that is known by the sender and the receiver. 
>![URI](http://i68.tinypic.com/ncgk04.png)
* JWT's are encoded with base64Url.
>![URI](http://i67.tinypic.com/2ngwydg.png)  
### Authentication/Authorization ###
* Authorization in API's are important because it helps secure private infromation.
* Before a user has access to an API the user usually has to apply for an application key.
* A user receives this key when they create an account.
* One way a user can get authorization from an API is through basic authorization.
* Basic authorization requires only a username and a password. When a user submits this information the username
and password then gets put together into one string. This string then gets encrypted and sent to the server. When 
the server receives this string it looks to see if it matches a stored key. If it matches then the user will have access to the API.
* This can also be done in POSTMAN. If you open postman and set the request to GET and change your authorization type to Basic Auth.
>![URI](http://i64.tinypic.com/245eqtz.png)
* Then fill out the Username and Password.
>![URI](http://i68.tinypic.com/iemgro.png)
* Press update request.
>![URI](http://i67.tinypic.com/2iutilu.png)
* Postman will then encrypt your information with Base64.
>![URI](http://i64.tinypic.com/mhud5h.png)
* Another method of authentication is OAuth 2.0
* OAuth 2.0 works by 
1. The user starting by requesting to connect to a server.
2. The server sends the user to the callback URL.
3. The user logs into the server with their username and password.
4. The server sends the user back to the callback URL with a hidden code.
5. After this the server approves the code then the server responds with an access token.
6. The user is now able to access the server.


### Postman ###

* Postman is a free user friendly API development tool.
* Postman is used to help building and test APIs.
* To acquire Postman [click here][id1].
* Postman can be used to easily execute HTTP requests.
* Postman can be used to create data by using the POST request. 
* To do this you would start off by simply opening up Postman.
>![URI](http://i67.tinypic.com/20poq4n.png)
* Click to change your HTTP request from GET to POST.
>![URI](http://i67.tinypic.com/d49kk.png)
* Change your Authorization type from No Authorization to OAuth 1.0.
>![URI](http://i66.tinypic.com/2wdr5gk.png)
* Then fill out the information that is provided by your developer account.
>![URI](http://i64.tinypic.com/2n685lw.png)
* After this click to add Add params to header.
>![URI](http://i63.tinypic.com/1emoec.png)
* Then fill in URL for your request.
>![URI](http://i65.tinypic.com/2u44y2a.png)
* Click the params button to add params for your request.
>![URI](http://i64.tinypic.com/1rzypj.png)
* For this example the params needed are a Username and the status.
>![URI](http://i66.tinypic.com/fd97rt.png)
* Then click the submit button.
>![URI](http://i65.tinypic.com/121al1t.png)
* Postman will then display the response and you can see the new status update and all of the details.
>![URI](http://i67.tinypic.com/wakign.png)
* You can also go to the webpage to see the new update from the POST request.
>![URI](http://i65.tinypic.com/28994p5.png)  

### Fiddler
>The free web debugging proxy for any browser, system and platform.  

[Learn more](http://www.telerik.com/fiddler)  

[id]: http://www.restapitutorial.com/httpstatuscodes.html
[id1]: https://www.getpostman.com/